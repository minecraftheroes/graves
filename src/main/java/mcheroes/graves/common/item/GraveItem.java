package mcheroes.graves.common.item;

import mcheroes.graves.common.block.GraveBlock;
import mcheroes.graves.common.block.ModBlocks;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.EmptyBlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BedPart;

import static mcheroes.graves.common.block.GraveBlock.PART;

public class GraveItem extends Item {

    public GraveItem()
    {
        super(new Item.Properties()
                .stacksTo(1)
                .tab(CreativeModeTab.TAB_DECORATIONS)
        );
    }

    @Override
    public InteractionResult onItemUseFirst(ItemStack stack, UseOnContext context) {

        // We can only support right-clicking the top of a block.
        if (context.getClickedFace() != Direction.UP)
            return InteractionResult.FAIL;

        BlockPos footPosition = context.getClickedPos();
        BlockState blockState = context.getLevel().getBlockState(footPosition);

        // If the clicked block cannot be replaced, try the block above.
        // For example, tall grass can be replaced.
        boolean footOldBlockReplaceable = blockState.canBeReplaced(new BlockPlaceContext(context));
        if (!footOldBlockReplaceable)
        {
            footPosition = footPosition.above();
        }

        BlockPos headPosition = footPosition.relative(context.getHorizontalDirection());

        // Check if the player is allowed place the block on top of the two head & foot block positions.
        if (context.getPlayer().mayUseItemAt(footPosition, context.getHorizontalDirection(), stack) &&
            context.getPlayer().mayUseItemAt(headPosition, context.getHorizontalDirection(), stack))
        {
            BlockState headBlockState = context.getLevel().getBlockState(headPosition);
            boolean headOldBlockReplaceable = headBlockState.canBeReplaced(new BlockPlaceContext(context));
            boolean canPlaceFoot = footOldBlockReplaceable || context.getLevel().getBlockState(footPosition).isAir();
            boolean canPlaceHead = headOldBlockReplaceable ||  context.getLevel().getBlockState(headPosition).isAir();

//            Block.canSupportRigidBlock(EmptyBlockGetter.INSTANCE, footPosition.below()) &&
//            Block.canSupportRigidBlock(EmptyBlockGetter.INSTANCE, headPosition.below())
            if (canPlaceHead && canPlaceFoot)
            {
                // todo place grave.
                BlockState defaultGraveState = ModBlocks.GRAVE.get().defaultBlockState();
                context.getLevel().setBlock(headPosition, defaultGraveState
                        .setValue(GraveBlock.PART, BedPart.HEAD)
                        .setValue(GraveBlock.FACING, context.getHorizontalDirection()), 10);
                context.getLevel().setBlock(footPosition, defaultGraveState
                        .setValue(PART, BedPart.FOOT)
                        .setValue(GraveBlock.FACING, context.getHorizontalDirection()), 10);

                SoundType soundType = defaultGraveState.getSoundType(context.getLevel(), footPosition, context.getPlayer());
                context.getLevel().playSound(context.getPlayer(), footPosition, soundType.getPlaceSound(), SoundSource.BLOCKS, soundType.getVolume(), soundType.getPitch() * 0.8F);

                if (!context.getPlayer().isCreative()) {
                    stack.shrink(1);
                }

                return InteractionResult.PASS;
            }
        }

        return InteractionResult.FAIL;
    }
}
