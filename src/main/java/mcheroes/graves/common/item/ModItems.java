package mcheroes.graves.common.item;

import mcheroes.graves.GravesMod;
import net.minecraft.world.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fmllegacy.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModItems {

    private static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, GravesMod.MOD_ID);

    public static final RegistryObject<Item> GRAVE = ITEMS.register("grave", () -> new GraveItem());

    public static void register(IEventBus eventBus)
    {
        ITEMS.register(eventBus);
    }
}
