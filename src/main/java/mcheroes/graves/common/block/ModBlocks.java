package mcheroes.graves.common.block;

import mcheroes.graves.GravesMod;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fmllegacy.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ModBlocks {

    private static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, GravesMod.MOD_ID);

    public static final RegistryObject<Block> GRAVE = BLOCKS.register("grave", () -> new GraveBlock());

    public static void register(IEventBus eventBus)
    {
        BLOCKS.register(eventBus);
    }
}
