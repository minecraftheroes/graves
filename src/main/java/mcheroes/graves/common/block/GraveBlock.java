package mcheroes.graves.common.block;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.ChestBlockEntity;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BedPart;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class GraveBlock extends Block {

    public static final EnumProperty<BedPart> PART = BlockStateProperties.BED_PART;
    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    protected static final VoxelShape AABB = Block.box(0.0D, 0.0D, 0.0D, 16.0D, 14.0D, 16.0D);

    public GraveBlock()
    {
        super(BlockBehaviour.Properties.of(Material.STONE).strength(3.5f));
        this.registerDefaultState(this.stateDefinition.any().setValue(PART, BedPart.FOOT));
    }

//    @Override
//    public BlockEntity newBlockEntity(BlockPos pos, BlockState blockState) {
//        return new ChestBlockEntity(pos, blockState);
//    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        pBuilder.add(FACING, PART);
    }

    //onNeighborChange


//    /**
//     * Copied from HorizontalDirectionBlock
//     * Returns the blockstate with the given rotation from the passed blockstate. If inapplicable, returns the passed
//     * blockstate.
//     * @deprecated call via {@link net.minecraft.world.level.block.state.BlockBehavior.BlockStateBase#rotate} whenever
//     * possible. Implementing/overriding is fine.
//     */
//    public BlockState rotate(BlockState pState, Rotation pRot) {
//        return pState.setValue(FACING, pRot.rotate(pState.getValue(FACING)));
//    }
//
//    /**
//     * Copied from HorizontalDirectionBlock
//     * Returns the blockstate with the given mirror of the passed blockstate. If inapplicable, returns the passed
//     * blockstate.
//     * @deprecated call via {@link net.minecraft.world.level.block.state.BlockBehavior.BlockStateBase#mirror} whenever
//     * possible. Implementing/overriding is fine.
//     */
//    public BlockState mirror(BlockState pState, Mirror pMirror) {
//        return pState.rotate(pMirror.getRotation(pState.getValue(FACING)));
//    }

    @Override
    public VoxelShape getShape(BlockState p_51569_, BlockGetter p_51570_, BlockPos p_51571_, CollisionContext p_51572_) {
        return AABB;
    }


    /**
     * Update the provided state given the provided neighbor direction and neighbor state, returning a new state.
     * For example, fences make their connections to the passed in state if possible, and wet concrete powder immediately
     * returns its solidified counterpart.
     * Note that this method should ideally consider only the specific direction passed in.
     * @param pState - the current state of this block
     * @param neighborDirection - the direction from this block to the neighbor triggering the update
     * @param neighborState - the current state of the neighboring block
     * @param pos - position of this block
     * @param neighborPos - position of the neighboring block
     */
    public BlockState updateShape(BlockState pState, Direction neighborDirection, BlockState neighborState, LevelAccessor pLevel, BlockPos pos, BlockPos neighborPos) {
        // Check if the neighboring block is the other part (head or foot)
        if (neighborDirection == getNeighborDirection(pState.getValue(PART), pState.getValue(FACING)))
        {
            // If the neighbor was changed to air, set this block also to air.
            if (neighborState.isAir()) {
                return Blocks.AIR.defaultBlockState();
            }
            else
            {
                return pState; // no change.
            }
        }
        else
        {
            return super.updateShape(pState, neighborDirection, neighborState, pLevel, pos, neighborPos);
        }
    }


    /**
     * Given a bed part and the direction it's facing, find the direction to move to get the other bed part
     */
    private static Direction getNeighborDirection(BedPart pPart, Direction pDirection) {
        // e.g.
        // if we are a foot, and facing west, move west to get to the head
        // if we are a head, and facing west, move east to get to the foot
        return pPart == BedPart.FOOT ? pDirection : pDirection.getOpposite();
    }
}
