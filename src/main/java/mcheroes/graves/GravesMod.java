package mcheroes.graves;

import mcheroes.graves.common.block.ModBlocks;
import mcheroes.graves.common.item.ModItems;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(GravesMod.MOD_ID)
public class GravesMod {

    public static final String MOD_ID = "graves";

    private static final Logger LOGGER = LogManager.getLogger();

//    public static CommonProxy proxy = DistExecutor.safeRunForDist(
//            () -> ClientProxy::new,
//            () -> CommonProxy::new
//    );

    public GravesMod()
    {
        IEventBus eventBus = FMLJavaModLoadingContext.get().getModEventBus();

        LOGGER.info("Setting up items and blocks.");
        ModItems.register(eventBus);
        ModBlocks.register(eventBus);

        eventBus.addListener(this::setup);
    }

    private void setup(final FMLCommonSetupEvent event)
    {

    }
}
